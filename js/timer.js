(function(){
  var pDay = document.getElementById("day"),
      pHour= document.getElementById('hour'),
      pMin = document.getElementById('min'),
      pSec = document.getElementById('sec');
  function contDown(){
    var now= new Date();
    var finish= new Date(now.getFullYear(), 11, 31);

    var nowTime= now.getTime();
    var finTime= finish.getTime();

    var remTime= finTime - nowTime;

    var s= Math.floor(remTime / 1000),
        m= Math.floor(s/60),
        h= Math.floor(m/60),
        d= Math.floor(h/24);

        h %= 24;
        m %= 60;
        s %= 60;

        pDay.innerText= d + "Días";
        if(h<10){h= "0"+h;}
        if(m<10){m= "0"+h;}
        if(s<10){s= "0"+s;}
        pHour.innerText= h;
        pMin.innerText= m;
        pSec.innerText= s;
  }
  contDown();
  setInterval(contDown, 1000);
})();
